import {DynamoDB} from "aws-sdk"
import {ClientConfig} from "aws-sdk"

declare type hash = any
declare type range = any

type ReturnValues = "NONE" | "ALL_NEW" | "ALL_OLD"

type errCallback = (error: Error) => void;
type callback<T> = (error: Error, data?: T) => void;
type itemsCallback = (error: Error, data?: vogels.Items) => void;
type batchItemCallback = (err: Error, items: vogels.Item[]) => void;
type nameFunc = () => string

type loglevel = "trace" | "debug" | "info" | "warn" | "error" | "fatal";
type indexType = "global" | "local"
type projectionType = "INCLUDE" | "ALL"


declare namespace vogels.interfaces {
    type CreateTablesOptions = { [key: string]: Throughput };

    interface ModelDefinition {
        hashKey: string
        rangeKey?: string
        timestamps?: boolean
        createdAt?: string,
        updatedAt?: string,
        schema: any
        tableName?: string | nameFunc;
        indexes?: Index[]
    }

    interface Projection {
        NonKeyAttributes: string[]
        ProjectionType?: projectionType
    }

    interface Index {
        hashKey: string,
        rangeKey?: string,
        name: string,
        type: indexType
        projection?: Projection
    }

    interface Throughput {
        readCapacity: number,
        writeCapacity: number
    }

    interface GetItemOptions {
        ConsistentRead?: boolean
        AttributesToGet?: string[]
        ProjectionExpression?: string
    }

    interface ModelConfig {
        tableName?: string
        dynamodb?: DynamoDB
    }

    interface UpdateExpression {
        ConditionExpression: string
        ExpressionAttributeNames: { [key: string]: string }
        ExpressionAttributeValues: { [key: string]: any }
    }

    interface UpdateOptions {
        ReturnValues?: ReturnValues
        expected?: { [key: string]: any }
    }

    interface Condition<T> {
        equals(value: any): T
        lte(value: any): T
        lt(value: any): T
        gt(value: any): T
        gte(value: any): T
        beginsWith(value: any): T
        between(from: any, to: any): T
    }

    interface ExpressionAPI<T> {
        filterExpression(expr: string): T
        expressionAttributeNames(names: { [key: string]: string }): T
        expressionAttributeValues(values: { [key: string]: any }): T
        projectionExpression(expr: string): T
    }

    interface Query extends ExpressionAPI<Query> {
        loadAll(): Query
        limit(limit: number): Query
        where(name: string): Condition<Query>
        attributes(attrs: string[]): Query
        exec(callback: itemsCallback): void
        ascending(): Query
        descending(): Query
        filter(name: string): Condition<Query>

        usingIndex(name: string): Query
    }

    interface ScanKeyCondition extends Condition<Scan> {
        notNull(): Scan
        contains(value: any): Scan
        notContains(value: any): Scan
        in(value: string[]): Scan

    }

    interface Scan extends ExpressionAPI<Scan> {
        loadAll(): Scan
        limit(limit: number): Scan
        exec(callback: itemsCallback): void
        where(name: string): ScanKeyCondition
    }

    interface Attribute {
        AttributeName: string,
        AttributeType: string
    }

    interface Index {
        IndexName: string,
        KeySchema: Attribute[],
        Projection: {
            ProjectionType: string
        }
        IndexSizeByptes: number,
        ItemCount: number,
        IndexArn: string
    }

    interface DescribeTableData {
        Table: {
            TableName: string,
            KeySchema: Attribute[]
            AttributeDefinitions: Attribute[]
            TableStatus: string,
            CreationDateTime: string,
            ProvisionedThroughput: {
                LastIncreaseDateTime: Date,
                LastDecreaseDateTime: Date,
                NumberOfDecreasesToday: number,
                ReadCapacityUnits: number,
                WriteCapacityUnits: number
            },
            TableSizeBytes: number,
            ItemCount: number,
            TableArn: string,
            LocalSecondaryIndexes?: Index[]
            GlobalSecondaryIndexes?: Index[]
        }
    }

    interface Model {
        deleteTable(callback: errCallback): void
        config(cfg: interfaces.ModelConfig): void

        create<T>(obj: T, callback: callback<Item>): void
        create<T>(obj: T, expr: interfaces.UpdateExpression, callback: callback<Item>): void
        create<T>(obj: T[], callback: callback<Item>): void
        save(callback: callback<Item>): void

        update<T>(obj: T, callback: callback<Item>): void
        update<T>(obj: T, options: interfaces.UpdateOptions, callback: callback<Item>): void
        update<T>(obj: T, expr: interfaces.UpdateExpression, callback: callback<Item>): void

        destroy(hash: any, callback: callback<Item>): void
        destroy(hash: any, options: interfaces.UpdateOptions, callback: callback<Item>): void
        destroy(hash: any, range: any, callback: callback<Item>): void
        destroy(hash: any, range: any, options: interfaces.UpdateOptions, callback: callback<Item>): void

        get(hash: hash, callback: callback<Item>): void
        get(hash: hash, range: range, callback: callback<Item>): void
        get(hash: hash, options: interfaces.GetItemOptions, callback: callback<Item>): void
        get(hash: hash, range: range, options: interfaces.GetItemOptions, callback: callback<Item>): void

        getItems(keys: hash[], options: interfaces.GetItemOptions, callback: batchItemCallback): void
        getItems(keys: hash[], callback: batchItemCallback): void
        getItems(keys: { [key: string]: string }[], callback: batchItemCallback): void
        getItems(keys: { [key: string]: string }[], options: interfaces.GetItemOptions, callback: batchItemCallback): void
        query(hash: any): interfaces.Query
        scan(): interfaces.Scan
        parallelScan(segments: number): interfaces.Scan

        createTable(callback: errCallback): void
        createTable(options: interfaces.Throughput, callback: errCallback): void

        updateTable(callback: errCallback): void
        updateTable(throughput: interfaces.Throughput, callback: errCallback): void

        describeTable(callback: callback<interfaces.DescribeTableData>): void;

        deleteTable(callback: errCallback): void

        log: log
    }
}

declare namespace vogels {
    function dynamoDriver(driver: DynamoDB): void

    module AWS {
        var config: ClientConfig
    }

    module types {
        function uuid(): string;
        function stringSet(): any;
        function numberSet(): any;
        function binarySet(): any;
    }

    var log: log

    interface Item {
        get(key: string): any
        get(): any
    }

    interface Items {
        Items: Item[]
    }

    function define(name: string, config: interfaces.ModelDefinition): interfaces.Model

    function createTables(callback: callback<any>): void

    function createTables(options: interfaces.CreateTablesOptions, callback: callback<any>): void;

}

interface log {
    level(level: loglevel): void
}

export = vogels;