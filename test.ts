/// <reference path="./build/main.d.ts" />
import * as aws from 'aws-sdk'
import * as vogels from 'vogels';
import {DynamoDB} from "aws-sdk"

vogels.define("name", {
    hashKey: "id",
    tableName: "test",
    schema: {}
});

vogels.AWS.config.update({ region: 'us-east-1'});
// var driver = new DynamoDB({endpoint : "http://localhost:8000"})

// vogels.dynamoDriver(driver)
// let piet = vogels.interfaces.Model;



let model = vogels.define("test21", {
    hashKey: "hash",
    updatedAt : "test",
    tableName: function(): string { return "tablename" },
    schema: {}
})

model.config({ tableName: "test123" })

model.describeTable((err, data) => {
    
})


class Person {
    name: string
}

var p = new Person()

model.create<Person>(p, (err) => { });

model.create<Person>([new Person(), new Person()], (err) => { })

var params: any = {
    ConditionExpression: '#i <> :x',
    ExpressionAttributeNames: { '#i': 'id' },
    ExpressionAttributeValues: { ':x': 123 }
}

model.create<Person>(new Person(), params, (err) => { })

model.update<Person>(new Person(), (err) => { });
model.update<Person>(new Person(), { ReturnValues: "ALL_NEW" }, (err) => { });
model.update<Person>(new Person(), { expected: { "age": 21 } }, (err) => { });

model.destroy("hash", (err) => { })
model.destroy("hash", params, (err) => { })
model.destroy("hash", { ReturnValues: "ALL_NEW" }, (err) => { });
model.destroy("hash", { expected: { "age": 21 } }, (err) => { });

model.get("hash", (err, item) => {
    if (err === null) {
        item.get("name")
    }
})

model.updateTable({writeCapacity : 1, readCapacity: 1}, (err) => {});

model.query("hash").filter("field").equals("value");

model.scan().where("name").equals("Werner").where("age").notNull().exec((err, data) => {});

model.getItems(["a", "b"], (err, items) => {
    
})
  
vogels.log.level("info");
model.log.level("warn");

model.getItems([{emal: "test123@example.com"}], (err, data) => {
    
})



vogels.createTables((err) => { });

var cfg: any  = { "test": { writeCapacity: 10 } }

vogels.createTables(cfg, (err: Error) => {

});







